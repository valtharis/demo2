import sys

import requests


def printing():
    if len(sys.argv) > 1:
        for arg in sys.argv[1:]:
            print(arg)
    else:
        print("Works!")


class Quote:
    def __init__(self, dict):
        self.__id = dict['ID']
        self.__title = dict['title']
        self.__content = dict['content']
        self.__link = dict['link']

    def __remove_html_tags(self, string):
        import re
        clean = re.compile('<.*?>')
        return re.sub(clean, '', string)

    def __str__(self):
        return str.format("{}:{}", self.__title, self.__remove_html_tags(self.__content))


response = requests.get('http://quotesondesign.com/wp-json/posts?filter[orderby]=rand')
q = response.json()

quotes = list()
if len(q) > 0:
    for quote in q:
        quotes.append(Quote(quote))

for quote in quotes:
    print(quote)


def getQuote():
    response = requests.get('http://quotesondesign.com/wp-json/posts?filter[orderby]=rand')
    quote = response.json()
    return Quote(quote[0])

smt =1