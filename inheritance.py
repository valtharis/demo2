
class Figura:
    def obowd(self):
        return 0

    def pole(self):
        return 0

class Trojkat(Figura):
    def __init__(self, a, b, h):
        self.a = a
        self.b = b
        self.h = h

    def obowd(self):
        return self.a+self.b+self.h

    def pole(self):
        return self.a/2*self.h

class Kwadrat(Figura):
    def __init__(self, a):
        self.a = a

    def obowd(self):
        return self.a*4

    def pole(self):
        return self.a*self.a


class Prostokat(Figura):
    def __init__(self, a, b):
        self.a = a
        self.b = b

    def obowd(self):
        return (self.a + self.b)*2

    def pole(self):
        return self.a * self.b


def obliczPole(figura):
    print(figura.pole())

def obliczObwod(figura):
    print(figura.obowd())


trojkat = Trojkat(2,3,4)
kwadrat = Kwadrat(3)
prostokat = Prostokat(2,4)

obliczObwod(trojkat)
obliczObwod(kwadrat)
obliczObwod(prostokat)


obliczPole(trojkat)
obliczPole(kwadrat)
obliczPole(prostokat)