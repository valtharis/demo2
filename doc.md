####Ogólne (50m)
* program, proces, wątek (3m)
* argumenty skryptu (5m)
* lista, słownik, tupla (15m)
* pętle, warunki (15m)
* debugger (5m)
* metody (5m)
####Kontrola wersji (GIT) (30m)
* konfiguracja (5m)
* wersjonowanie plików (3m)
* commit (3m)
* remote, push, pull (15m)
* branch* (10m)
* konflikty* (10m)
####OOP (30m)
* instancja a klasa, konstruktor (15m)
* dziedziczenie (5m)
* enkapsulacja, accessory, property (15m)
####HTTP (30m)
* request, metody, statusy (10m)
* json (10m)
####DB (30m)
* connector (5m)
* baza, tabela, relacja* (10m)
* insert (10m)
* select (10m)
####OOP*
* SOLID
* wzorce

* element
* drugi element listy
