import mysql.connector
import random


class Article:
    def __init__(self, title, content):
        self.__id = None
        self.__title = title
        self.__content = content

    @property
    def id(self):
        return self.__id

    @id.setter
    def id(self, id):
        self.__id = id

    @property
    def title(self):
        return self.__title

    @property
    def content(self):
        return self.__content

    def __str__(self):
        return str.format('{} {} {}', self.__id, self.__title, self.__content)



class ArticleFactory:
    def build(self, id, title, content):
        article = Article(title, content)
        article.id = id
        return article



connection = mysql.connector.connect(
    host="localhost",
    user="dbuser",
    passwd="dbpass",
    database="demo"
)

stmt = connection.cursor()
sql = "SELECT * FROM articles"
stmt.execute(sql)
articles = stmt.fetchall()

articleFactory = ArticleFactory()

for article in articles:
    print(articleFactory.build(id=article[0], title=article[1], content=article[2]))

'''

'''




sql = "INSERT INTO articles (title, content) VALUES (%s, %s)"

article1 = (u'Artykuł ' + str(random.randint(0, 999)), u'Treść artykułu' )

stmt.execute(sql, article1)
connection.commit()

article2 = Article(u'Artykuł ' + str(random.randint(0,999)) , u'Treść artykułu' )

stmt.execute(sql, (article2.title, article2.content))
connection.commit()

a = (3, u'łąka', 3.456)
print(a)




