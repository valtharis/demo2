from flask import Flask, render_template, request

from main import getQuote

app = Flask(__name__)




@app.route("/", methods=['GET'])
def index():
    quote = getQuote()
    return render_template('index.html', quote=quote)

@app.route("/", methods=['POST'])
def form():
    data = request.form
    return render_template('index.html')


if __name__ == "__main__":
    app.debug = True
    app.run(host='localhost', port=5001)

